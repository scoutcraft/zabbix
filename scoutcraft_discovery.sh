#!/bin/bash
servers=$(grep -r "motd" /srv/daemon-data/*/server.properties)
serversJson=''

for i in $servers;
do 
	serverId=$(echo "$i" | cut -d '/' -f 4)
	serverName=$(echo "$i" | cut -d '=' -f 2 | sed 's/\\//')
	serverPath=$(echo "$i" | cut -d ':' -f 1 | sed 's/server.properties//')
	serverPort=$(cat $serverPath/server.properties | grep server-port | cut -d '=' -f 2)
	serversJson="$serversJson,{\"serverId\":\"$serverId\",\"serverName\":\"$serverName\",\"serverPath\":\"$serverPath\",\"serverPort\":\"$serverPort\"}"
done

result=$(echo $serversJson | sed 's/^,//g')
echo "[$result]"
